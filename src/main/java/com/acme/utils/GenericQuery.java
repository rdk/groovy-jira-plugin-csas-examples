package com.acme.utils;

import com.atlassian.jira.component.ComponentAccessor;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityExprList;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class GenericQuery {
	
	private List<EntityExpr> list = new LinkedList<EntityExpr>();
	
	public GenericQuery addExpr(String fieldName, EntityOperator opertor, Object value) {
		list.add(new EntityExpr(fieldName, opertor, value));
		return this;
	}
	
	public GenericQuery addEquals(String fieldName, Object value) {
		addExpr(fieldName, EntityOperator.EQUALS, value);
		return this;
	}
	
	public EntityExprList  getAndList() {
		return new EntityExprList(list, EntityOperator.AND);
	}
	
	public EntityExprList  getOrList() {
		return new EntityExprList(list, EntityOperator.OR);
	}
	
    public List<EntityExpr> getList() {
		return list;
	}
    
    public static GenericQuery where() {
    	return new GenericQuery();
    }
    
//================================================================================================//
//    
//    @SuppressWarnings("unchecked")
//	public List<GenericValue> findByAnd(String entityName) {
//    	try {
//			return CoreFactory.getGenericDelegator().findByAnd(entityName, getList());
//		} catch (GenericEntityException e) {
//			log.error(e.getMessage(), e);
//			throw new DataAccessException(e);
//		}
//    }
    
	public List<GenericValue> findByAnd(String entityName, List<String> fieldsToSelect, List<String> orderBy) {
		return ComponentAccessor.getOfBizDelegator().findByCondition(entityName, getAndList(), fieldsToSelect, orderBy);
    }
    
    
//================================================================================================//
    
    private static final Logger log = LoggerFactory.getLogger(GenericQuery.class);
	
}
