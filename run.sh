#!/bin/bash

export JAVA_HOME="C:\Program Files\Java\jdk1.7.0_67"

"$JAVA_HOME/bin/java" -version

cmd //C atlas-run.bat --jvmargs '-Xmx1512m -XX:MaxPermSize=256m' "$@" | logc
